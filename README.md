# Detecção de Cópias de Imagens baseada em similaridade de conteúdo

Algoritmo usado para avaliação do desempenho da solução apresentada na monografia do meu TCC (master). Também serve de repositório para alterações do código fonte original à medida que surgem novas técnicas e tecnologias (develop).

# Versão Original

Na branch master. Contém a versão original usada para a defesa da monografia. Não deve ser alterada, pois serve de amostra.

# Versão em desenvolvimento

Na branch develop. Essa poderá ser alterada, tentando buscar um balanço justo no tradeoff de design e performance.
