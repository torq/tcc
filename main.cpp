#include <iostream>
#include <math.h>
#include <fstream>
#include "opencv2/opencv.hpp"

#define QSIZE = 12;
#define ACS_DIM = 12;
#define ACS = 143;
#define THRESHOLD = 0.32;

using namespace std;
using namespace cv;

Mat getSubImage (Mat image) {
    Mat y;
    image.convertTo(y, CV_32F);
    Mat avg(QSIZE, QSIZE, CV_32FC1);
    int sizeY = y.rows/QSIZE;
    int sizeX = y.cols/QSIZE;
    for (size_t i=0, k=0; i < y.rows && k<QSIZE; i+=sizeY, ++k) {
        if (i + sizeY > y.rows) continue;
        float* avg_ptr = avg.ptr<float>(k) ;
        for (size_t j=0, l=0; j<y.cols && l<QSIZE ; j += sizeX, ++l) {
            if (j+sizeX > y.cols) continue;
            Mat roi = y(Rect( j , i , sizeX , sizeY)) ;
            avg_ptr[l] = (float) mean(roi).val[0];
        }
    }
    return avg ;
}

Mat ordinalMeasureAC (Mat image ) {
    Mat selected(1, ACS, CV_32F);
    int c = 0;
    float* selected_ptr = selected.ptr<float>(0);
    for (size_t i = 0; i < ACS_DIM; ++i) {
        const float* image_ptr = image.ptr<float>(i) ;
        for (size_t j = 0; j < ACS_DIM; ++j)
            if (i || j) selected_ptr[c++] = image_ptr[j];
    }
    return selected;
}

Mat processImage(Mat image) {
    Mat sub = getSubImage(image);
    Mat dcted;
    dct(image, dcted);
    Mat acs = ordinalMeasureAC(dcted);
    return acs;
}

double getSM (Mat avComp1, Mat avComp2 ) {
    float M = 0.0;
    for (size_t i = 0; i < avComp1.rows; ++i) {
        float* avComp1_ptr = avComp1.ptr<float>(i);
        float* avComp2_ptr = avComp2.ptr<float>(i);
        for (size_t j = 0; j < avComp1.cols; ++j)
            M += fabs(avComp1_ptr[j] - avComp2_ptr[avComp1.cols-j-1]);
    }

    return norm(avComp1, avComp2, NORM_L1)/M;
}

int main (int argc, char** argv) {
    if (argc != 3) {
        cout << "Wrong Usage: <exec> <query image> <database path>" << endl;
        return 0;
    }

    Mat image1 = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    if (!image1.data) {
        cout << "Ocorreu um erro ao carregar a imagem." << endl;
        return 1;
    }

    string path(argv[2]);
    ofstream file;
    file.open("result.txt", ios::out | ios::app);

    int c = 0;
    FileStorage fs(path+"parameters.xml", FileStorage::READ);
    if (!fs.isOpened()) {
        cout << "Could not open parameters." << endl;
        exit(0);
    }
    Mat avComp1 = processImage(image1);
    while (true) {
        try{
            Mat avComp2(1, ACS, CV_32F);
            string count = static_cast<ostringstream*>(&(ostringstream() << c++))->str();
            count = " imagem " + count;
            FileNode node = fs[count];
            string name = node["name"];
            node["Matriz"] >> avComp2;

            if (name == " ") break;


            double n = getSM (avComp1, avComp2);
            if (n < THRESHOLD) file << name << " \n ";
        }catch (...) {
            cout << " Ocorreu um  erro na leitura do arquivo de database." << endl;
            break;
        }
    }

    fs.release();
    file.close();
    return 0;
}
